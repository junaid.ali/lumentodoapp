<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Users;

class UsersController extends Controller {

    public function __construct() {
        //  $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request) {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        $user = Users::where('email', $request->input('email'))->first();
        $hashed = Hash::make($request->input('password'));
        if (Hash::check($request->input('password'), $user->password)) {
            $apikey = base64_encode(md5($request->input('email') . date("YmdHis")));
            Users::where('email', $request->input('email'))->update(['api_key' => "$apikey"]);
            return response()->json(['status' => 'success', 'api_key' => $apikey]);
        } else {
            return response()->json(['status' => 'fail'], 401);
        }
    }

    public function store(Request $request) {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_no' => 'required',
            'gender' => 'required',
            'date_of_birth' => 'required',
        ]);
        $requestData = $request->all();

        $userCount = Users::where('email', $requestData['email'])->count();
        //$userCount = $user->count();
        if ($userCount > 0) {
            return response()->json(['status' => 'provided email is already regsitered']);
        }
        $requestData["password"] = Hash::make($request->input('password'));
        if (Users::Create($requestData)) {
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'fail', "wordCount" => $wordCount]);
        }
    }

}
